/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kla_klok;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import javax.swing.*;

/**
 *
 * @author Pangsora
 */
public class Kla_Klok_Form extends javax.swing.JFrame {

    /**
     * Creates new form Kla_Klok_Form
     */
    String drag, drop;
    Boolean edrag, edrop;
    Thread tkla, tklok, tmon, tfish, tkdam, tkea;
    JLabel[] lb1, lb2, lb3;
    int countUser = -1;

    public Kla_Klok_Form() {
        initComponents();
        //AddModel Use for Handle Drag And Drop Copy Value Type...In this case we copy name value
        AddModel();
        InitializeThread();
        InitializeJLabel();
        ShowHideLabel(lb1, false);
        ShowHideLabel(lb2, false);
        ShowHideLabel(lb3, false);
        ControlDice(false);
        janUp.setVisible(false);
        btnRoll.setEnabled(false);
        ThreadChecking.totalluyjak = 0;
    }

    public void showBtnRoll() {
        btnRoll.setEnabled(true);
    }

    private void ControlDice(boolean visible) {
        jan.setVisible(visible);
        dice_1.setVisible(visible);
        dice_2.setVisible(visible);
        dice_3.setVisible(visible);
    }

    public void NewUser(String UserName, long Money) {
        countUser++;
        //int x = 10, y = 30;
        switch (countUser) {
            case 0:
                SetValue(NameBoss, null, MoneyBoss, UserName, Money);
                ThreadChecking.luyboss = Money;
                break;
            case 1:
//                u1Name.setText(u1Name.getText() + UserName);
//                u1l.setText(UserName);
//                u1Money.setText(u1Money.getText() + Money);
//                Store Money to Name To use in ThreadChecking
//                u1Money.setName(Money + "");
                btnRoll.setEnabled(true);
                SetValue(u1Name, u1l, u1Money, UserName, Money);
                ShowHideLabel(lb1, true);
                break;
            case 2:
                SetValue(u2Name, u2l, u2Money, UserName, Money);
                ShowHideLabel(lb2, true);
                //JOptionPane.showMessageDialog(null, "Please Input Data!!!");
                break;
            case 3:
                SetValue(u3Name, u3l, u3Money, UserName, Money);
                ShowHideLabel(lb3, true);
                btnNewUser.setEnabled(false);
                break;
        }
//        for(int i=0;i<7;i++){
//            lb[countUser-1][i]=new JLabel();
//            lb[countUser-1][i].setText("0");
//            lb[countUser-1][i].setBounds(x, y, 50, 15);
//            add(lb[countUser-1][i]);
//            x+=60;
//        }
    }

    private void SetValue(JLabel UName, JLabel UTable, JLabel UMoney, String Name, long Money) {
        UName.setText(UName.getText() + Name);
        if (UTable != null) {
            UTable.setText(Name);
        }
        UMoney.setText(UMoney.getText() + Money);
        UMoney.setName(Money + "");
    }

    private void AddModel() {
        MouseListener ml = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                JComponent jc = (JComponent) e.getSource();
                //Condition Krao pi luy drag ot kert
                if (jc.getName() != "") {
                    TransferHandler th = jc.getTransferHandler();
                    th.exportAsDrag(jc, e, TransferHandler.COPY);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        };
        jLabel_Kla.addMouseListener(ml);
        jLabel_Klok.addMouseListener(ml);
        jLabel_Mon.addMouseListener(ml);
        jLabel_Kea.addMouseListener(ml);
        jLabel_Kdam.addMouseListener(ml);
        jLabel_Fish.addMouseListener(ml);
        jLabel_DL_1000.addMouseListener(ml);
        jLabel_DL_5000.addMouseListener(ml);
        jLabel_DL_20000.addMouseListener(ml);
        jLabel_LL_1000.addMouseListener(ml);
        jLabel_LL_5000.addMouseListener(ml);
        jLabel_LL_20000.addMouseListener(ml);
        jLabel_RL_1000.addMouseListener(ml);
        jLabel_RL_5000.addMouseListener(ml);
        jLabel_RL_20000.addMouseListener(ml);

        jLabel_Kla.setTransferHandler(new TransferHandler("name"));
        jLabel_Klok.setTransferHandler(new TransferHandler("name"));
        jLabel_Mon.setTransferHandler(new TransferHandler("name"));
        jLabel_Kea.setTransferHandler(new TransferHandler("name"));
        jLabel_Kdam.setTransferHandler(new TransferHandler("name"));
        jLabel_Fish.setTransferHandler(new TransferHandler("name"));
        jLabel_DL_1000.setTransferHandler(new TransferHandler("name"));
        jLabel_DL_5000.setTransferHandler(new TransferHandler("name"));
        jLabel_DL_20000.setTransferHandler(new TransferHandler("name"));
        jLabel_LL_1000.setTransferHandler(new TransferHandler("name"));
        jLabel_LL_5000.setTransferHandler(new TransferHandler("name"));
        jLabel_LL_20000.setTransferHandler(new TransferHandler("name"));
        jLabel_RL_1000.setTransferHandler(new TransferHandler("name"));
        jLabel_RL_5000.setTransferHandler(new TransferHandler("name"));
        jLabel_RL_20000.setTransferHandler(new TransferHandler("name"));

    }

    private void InitializeThread() {
        //Initialize Thread to Check User Jak ey ke
        tkla = ThreadChecking.MyThread(10, jLabel_Kla, 1);
        tklok = ThreadChecking.MyThread(10, jLabel_Klok, 2);
        tmon = ThreadChecking.MyThread(10, jLabel_Mon, 3);
        tkea = ThreadChecking.MyThread(10, jLabel_Kea, 4);
        tkdam = ThreadChecking.MyThread(10, jLabel_Kdam, 5);
        tfish = ThreadChecking.MyThread(10, jLabel_Fish, 6);
        tkla.start();
        tklok.start();
        tmon.start();
        tkea.start();
        tkdam.start();
        tfish.start();
        //End Initailize Thread
    }

    private void InitializeJLabel() {
        lb1 = new JLabel[]{u1l, u1l_tiger, u1l_gourd, u1l_chicken, u1l_Lobster, u1l_crab, u1l_fish, u1Money, u1Name, u1_chair, jLabel_DL_1000, jLabel_DL_5000, jLabel_DL_20000};
        ThreadChecking.lb[0] = lb1;
        lb2 = new JLabel[]{u2l, u2l_tiger, u2l_gourd, u2l_chicken, u2l_Lobster, u2l_crab, u2l_fish, u2Money, u2Name, u2_chair, jLabel_LL_1000, jLabel_LL_5000, jLabel_LL_20000};
        ThreadChecking.lb[1] = lb2;
        lb3 = new JLabel[]{u3l, u3l_tiger, u3l_gourd, u3l_chicken, u3l_Lobster, u3l_crab, u3l_fish, u3Money, u3Name, u3_chair, jLabel_RL_1000, jLabel_RL_5000, jLabel_RL_20000};
        ThreadChecking.lb[2] = lb3;
    }

    private void ShowHideLabel(JLabel[] label, boolean visible) {
        for (int i = 0; i < label.length; i++) {
            if (visible) {
                label[i].setVisible(true);
            } else {
                label[i].setVisible(false);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnNewUser = new javax.swing.JButton();
        btnRoll = new javax.swing.JButton();
        ChairBoss = new javax.swing.JLabel();
        NameBoss = new javax.swing.JLabel();
        MoneyBoss = new javax.swing.JLabel();
        janUp = new javax.swing.JLabel();
        dice_1 = new javax.swing.JLabel();
        dice_2 = new javax.swing.JLabel();
        dice_3 = new javax.swing.JLabel();
        jan = new javax.swing.JLabel();
        u3_chair = new javax.swing.JLabel();
        u3Name = new javax.swing.JLabel();
        u3Money = new javax.swing.JLabel();
        u3l = new javax.swing.JLabel();
        u3l_fish = new javax.swing.JLabel();
        u3l_crab = new javax.swing.JLabel();
        u3l_Lobster = new javax.swing.JLabel();
        u3l_chicken = new javax.swing.JLabel();
        u3l_gourd = new javax.swing.JLabel();
        u3l_tiger = new javax.swing.JLabel();
        u2_chair = new javax.swing.JLabel();
        u2Name = new javax.swing.JLabel();
        u2Money = new javax.swing.JLabel();
        u2l = new javax.swing.JLabel();
        u2l_fish = new javax.swing.JLabel();
        u2l_crab = new javax.swing.JLabel();
        u2l_Lobster = new javax.swing.JLabel();
        u2l_chicken = new javax.swing.JLabel();
        u2l_gourd = new javax.swing.JLabel();
        u2l_tiger = new javax.swing.JLabel();
        u1_chair = new javax.swing.JLabel();
        u1Money = new javax.swing.JLabel();
        u1Name = new javax.swing.JLabel();
        u1l = new javax.swing.JLabel();
        u1l_fish = new javax.swing.JLabel();
        u1l_crab = new javax.swing.JLabel();
        u1l_Lobster = new javax.swing.JLabel();
        u1l_chicken = new javax.swing.JLabel();
        u1l_gourd = new javax.swing.JLabel();
        u1l_tiger = new javax.swing.JLabel();
        Fish = new javax.swing.JLabel();
        Crab = new javax.swing.JLabel();
        Lobster = new javax.swing.JLabel();
        Chicken = new javax.swing.JLabel();
        Gourd = new javax.swing.JLabel();
        Tiger = new javax.swing.JLabel();
        jLabel_RL_1000 = new javax.swing.JLabel();
        jLabel_RL_5000 = new javax.swing.JLabel();
        jLabel_RL_20000 = new javax.swing.JLabel();
        jLabel_LL_1000 = new javax.swing.JLabel();
        jLabel_LL_5000 = new javax.swing.JLabel();
        jLabel_LL_20000 = new javax.swing.JLabel();
        jLabel_DL_20000 = new javax.swing.JLabel();
        jLabel_DL_5000 = new javax.swing.JLabel();
        jLabel_DL_1000 = new javax.swing.JLabel();
        jLabel_Kla = new javax.swing.JLabel();
        jLabel_Klok = new javax.swing.JLabel();
        jLabel_Mon = new javax.swing.JLabel();
        jLabel_Fish = new javax.swing.JLabel();
        jLabel_Kdam = new javax.swing.JLabel();
        jLabel_Kea = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(1920, 1080));

        jPanel1.setLayout(null);

        btnNewUser.setText("New User");
        btnNewUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewUserActionPerformed(evt);
            }
        });
        jPanel1.add(btnNewUser);
        btnNewUser.setBounds(1740, 40, 130, 32);

        btnRoll.setText("Roll");
        btnRoll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRollActionPerformed(evt);
            }
        });
        jPanel1.add(btnRoll);
        btnRoll.setBounds(1740, 90, 130, 32);

        ChairBoss.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChairBossMouseClicked(evt);
            }
        });
        jPanel1.add(ChairBoss);
        ChairBoss.setBounds(850, 50, 213, 204);

        NameBoss.setForeground(new java.awt.Color(255, 204, 0));
        NameBoss.setText("User Name : ");
        jPanel1.add(NameBoss);
        NameBoss.setBounds(1070, 110, 160, 16);

        MoneyBoss.setForeground(new java.awt.Color(255, 204, 0));
        MoneyBoss.setText("Money :");
        jPanel1.add(MoneyBoss);
        MoneyBoss.setBounds(1070, 140, 160, 16);

        janUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/Jan1.png"))); // NOI18N
        jPanel1.add(janUp);
        janUp.setBounds(800, 380, 305, 304);

        dice_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/d_Klok.png"))); // NOI18N
        jPanel1.add(dice_1);
        dice_1.setBounds(910, 420, 85, 99);

        dice_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/d_Klok.png"))); // NOI18N
        jPanel1.add(dice_2);
        dice_2.setBounds(970, 530, 85, 99);

        dice_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/d_Klok.png"))); // NOI18N
        jPanel1.add(dice_3);
        dice_3.setBounds(840, 530, 85, 99);

        jan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/jandown.png"))); // NOI18N
        jPanel1.add(jan);
        jan.setBounds(800, 380, 305, 304);

        u3_chair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/MR_Chair.png"))); // NOI18N
        u3_chair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                u3_chairMouseClicked(evt);
            }
        });
        jPanel1.add(u3_chair);
        u3_chair.setBounds(1500, 420, 213, 230);

        u3Name.setForeground(new java.awt.Color(0, 0, 0));
        u3Name.setText("User Name : ");
        jPanel1.add(u3Name);
        u3Name.setBounds(1520, 370, 160, 16);

        u3Money.setForeground(new java.awt.Color(0, 0, 0));
        u3Money.setText("Money :");
        jPanel1.add(u3Money);
        u3Money.setBounds(1520, 400, 160, 16);

        u3l.setForeground(new java.awt.Color(0, 0, 0));
        u3l.setText("User 2 :");
        jPanel1.add(u3l);
        u3l.setBounds(10, 70, 50, 16);

        u3l_fish.setForeground(new java.awt.Color(0, 0, 0));
        u3l_fish.setText("0");
        jPanel1.add(u3l_fish);
        u3l_fish.setBounds(370, 70, 50, 16);

        u3l_crab.setForeground(new java.awt.Color(0, 0, 0));
        u3l_crab.setText("0");
        jPanel1.add(u3l_crab);
        u3l_crab.setBounds(310, 70, 50, 16);

        u3l_Lobster.setForeground(new java.awt.Color(0, 0, 0));
        u3l_Lobster.setText("0");
        jPanel1.add(u3l_Lobster);
        u3l_Lobster.setBounds(250, 70, 50, 16);

        u3l_chicken.setForeground(new java.awt.Color(0, 0, 0));
        u3l_chicken.setText("0");
        jPanel1.add(u3l_chicken);
        u3l_chicken.setBounds(190, 70, 50, 16);

        u3l_gourd.setForeground(new java.awt.Color(0, 0, 0));
        u3l_gourd.setText("0");
        jPanel1.add(u3l_gourd);
        u3l_gourd.setBounds(130, 70, 50, 16);

        u3l_tiger.setForeground(new java.awt.Color(0, 0, 0));
        u3l_tiger.setText("0");
        jPanel1.add(u3l_tiger);
        u3l_tiger.setBounds(70, 70, 50, 16);

        u2_chair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_Chair.png"))); // NOI18N
        u2_chair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                u2_chairMouseClicked(evt);
            }
        });
        jPanel1.add(u2_chair);
        u2_chair.setBounds(210, 420, 213, 230);

        u2Name.setForeground(new java.awt.Color(0, 0, 0));
        u2Name.setText("User Name : ");
        jPanel1.add(u2Name);
        u2Name.setBounds(230, 370, 160, 16);

        u2Money.setForeground(new java.awt.Color(0, 0, 0));
        u2Money.setText("Money :");
        jPanel1.add(u2Money);
        u2Money.setBounds(230, 400, 160, 16);

        u2l.setForeground(new java.awt.Color(0, 0, 0));
        u2l.setText("User 2 :");
        jPanel1.add(u2l);
        u2l.setBounds(10, 50, 50, 16);

        u2l_fish.setForeground(new java.awt.Color(0, 0, 0));
        u2l_fish.setText("0");
        jPanel1.add(u2l_fish);
        u2l_fish.setBounds(370, 50, 50, 16);

        u2l_crab.setForeground(new java.awt.Color(0, 0, 0));
        u2l_crab.setText("0");
        jPanel1.add(u2l_crab);
        u2l_crab.setBounds(310, 50, 50, 16);

        u2l_Lobster.setForeground(new java.awt.Color(0, 0, 0));
        u2l_Lobster.setText("0");
        jPanel1.add(u2l_Lobster);
        u2l_Lobster.setBounds(250, 50, 50, 16);

        u2l_chicken.setForeground(new java.awt.Color(0, 0, 0));
        u2l_chicken.setText("0");
        jPanel1.add(u2l_chicken);
        u2l_chicken.setBounds(190, 50, 50, 16);

        u2l_gourd.setForeground(new java.awt.Color(0, 0, 0));
        u2l_gourd.setText("0");
        jPanel1.add(u2l_gourd);
        u2l_gourd.setBounds(130, 50, 50, 16);

        u2l_tiger.setForeground(new java.awt.Color(0, 0, 0));
        u2l_tiger.setText("0");
        jPanel1.add(u2l_tiger);
        u2l_tiger.setBounds(70, 50, 50, 16);

        u1_chair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/Down_Chair.png"))); // NOI18N
        u1_chair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                u1_chairMouseClicked(evt);
            }
        });
        jPanel1.add(u1_chair);
        u1_chair.setBounds(870, 830, 213, 204);

        u1Money.setForeground(new java.awt.Color(0, 0, 0));
        u1Money.setText("Money :");
        jPanel1.add(u1Money);
        u1Money.setBounds(1080, 970, 160, 16);

        u1Name.setForeground(new java.awt.Color(0, 0, 0));
        u1Name.setText("User Name : ");
        jPanel1.add(u1Name);
        u1Name.setBounds(1080, 940, 160, 16);

        u1l.setForeground(new java.awt.Color(0, 0, 0));
        u1l.setText("User 1 :");
        jPanel1.add(u1l);
        u1l.setBounds(10, 30, 50, 16);

        u1l_fish.setForeground(new java.awt.Color(0, 0, 0));
        u1l_fish.setText("0");
        jPanel1.add(u1l_fish);
        u1l_fish.setBounds(370, 30, 50, 16);

        u1l_crab.setForeground(new java.awt.Color(0, 0, 0));
        u1l_crab.setText("0");
        jPanel1.add(u1l_crab);
        u1l_crab.setBounds(310, 30, 50, 16);

        u1l_Lobster.setForeground(new java.awt.Color(0, 0, 0));
        u1l_Lobster.setText("0");
        jPanel1.add(u1l_Lobster);
        u1l_Lobster.setBounds(250, 30, 50, 16);

        u1l_chicken.setForeground(new java.awt.Color(0, 0, 0));
        u1l_chicken.setText("0");
        jPanel1.add(u1l_chicken);
        u1l_chicken.setBounds(190, 30, 50, 16);

        u1l_gourd.setForeground(new java.awt.Color(0, 0, 0));
        u1l_gourd.setText("0");
        jPanel1.add(u1l_gourd);
        u1l_gourd.setBounds(130, 30, 50, 16);

        u1l_tiger.setForeground(new java.awt.Color(0, 0, 0));
        u1l_tiger.setText("0");
        jPanel1.add(u1l_tiger);
        u1l_tiger.setBounds(70, 30, 50, 16);

        Fish.setForeground(new java.awt.Color(0, 0, 0));
        Fish.setText("Fish");
        jPanel1.add(Fish);
        Fish.setBounds(370, 10, 50, 16);

        Crab.setForeground(new java.awt.Color(0, 0, 0));
        Crab.setText("Crab");
        jPanel1.add(Crab);
        Crab.setBounds(310, 10, 50, 16);

        Lobster.setForeground(new java.awt.Color(0, 0, 0));
        Lobster.setText("Lobster");
        jPanel1.add(Lobster);
        Lobster.setBounds(250, 10, 50, 16);

        Chicken.setForeground(new java.awt.Color(0, 0, 0));
        Chicken.setText("Chicken");
        jPanel1.add(Chicken);
        Chicken.setBounds(190, 10, 50, 16);

        Gourd.setForeground(new java.awt.Color(0, 0, 0));
        Gourd.setText("Gourd");
        jPanel1.add(Gourd);
        Gourd.setBounds(130, 10, 50, 16);

        Tiger.setForeground(new java.awt.Color(0, 0, 0));
        Tiger.setText("Tiger");
        jPanel1.add(Tiger);
        Tiger.setBounds(70, 10, 50, 16);

        jLabel_RL_1000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_1000.png"))); // NOI18N
        jLabel_RL_1000.setText("​");
        jLabel_RL_1000.setName("3_1000"); // NOI18N
        jPanel1.add(jLabel_RL_1000);
        jLabel_RL_1000.setBounds(1360, 470, 79, 38);

        jLabel_RL_5000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_5000.png"))); // NOI18N
        jLabel_RL_5000.setText("​");
        jLabel_RL_5000.setName("3_5000"); // NOI18N
        jPanel1.add(jLabel_RL_5000);
        jLabel_RL_5000.setBounds(1360, 520, 79, 38);

        jLabel_RL_20000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_20000.png"))); // NOI18N
        jLabel_RL_20000.setText("​");
        jLabel_RL_20000.setName("3_20000"); // NOI18N
        jPanel1.add(jLabel_RL_20000);
        jLabel_RL_20000.setBounds(1360, 570, 79, 38);

        jLabel_LL_1000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_1000.png"))); // NOI18N
        jLabel_LL_1000.setText("​");
        jLabel_LL_1000.setName("2_1000"); // NOI18N
        jPanel1.add(jLabel_LL_1000);
        jLabel_LL_1000.setBounds(480, 470, 79, 38);

        jLabel_LL_5000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_5000.png"))); // NOI18N
        jLabel_LL_5000.setText("​");
        jLabel_LL_5000.setName("2_5000"); // NOI18N
        jPanel1.add(jLabel_LL_5000);
        jLabel_LL_5000.setBounds(480, 520, 79, 38);

        jLabel_LL_20000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/ML_20000.png"))); // NOI18N
        jLabel_LL_20000.setText("​");
        jLabel_LL_20000.setName("2_20000"); // NOI18N
        jPanel1.add(jLabel_LL_20000);
        jLabel_LL_20000.setBounds(480, 570, 79, 38);

        jLabel_DL_20000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/M_20000.png"))); // NOI18N
        jLabel_DL_20000.setText("​");
        jLabel_DL_20000.setName("1_20000"); // NOI18N
        jPanel1.add(jLabel_DL_20000);
        jLabel_DL_20000.setBounds(990, 690, 38, 79);

        jLabel_DL_5000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/M_5000.png"))); // NOI18N
        jLabel_DL_5000.setText("​");
        jLabel_DL_5000.setName("1_5000"); // NOI18N
        jPanel1.add(jLabel_DL_5000);
        jLabel_DL_5000.setBounds(940, 690, 38, 79);

        jLabel_DL_1000.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/M_1000.png"))); // NOI18N
        jLabel_DL_1000.setText("​");
        jLabel_DL_1000.setName("1_1000"); // NOI18N
        jPanel1.add(jLabel_DL_1000);
        jLabel_DL_1000.setBounds(890, 690, 38, 79);

        jLabel_Kla.setName(""); // NOI18N
        jPanel1.add(jLabel_Kla);
        jLabel_Kla.setBounds(670, 400, 190, 140);

        jLabel_Klok.setName(""); // NOI18N
        jPanel1.add(jLabel_Klok);
        jLabel_Klok.setBounds(870, 400, 195, 140);

        jLabel_Mon.setName(""); // NOI18N
        jPanel1.add(jLabel_Mon);
        jLabel_Mon.setBounds(1070, 400, 190, 140);

        jLabel_Fish.setName(""); // NOI18N
        jPanel1.add(jLabel_Fish);
        jLabel_Fish.setBounds(1070, 540, 190, 140);

        jLabel_Kdam.setName(""); // NOI18N
        jPanel1.add(jLabel_Kdam);
        jLabel_Kdam.setBounds(870, 540, 195, 140);

        jLabel_Kea.setName(""); // NOI18N
        jPanel1.add(jLabel_Kea);
        jLabel_Kea.setBounds(670, 540, 190, 140);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/kla_klok/table.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 1920, 1080);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1908, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1068, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewUserActionPerformed
        // TODO add your handling code here:
        //new Form_CreateUser().setVisible(true);
        Form_CreateUser fc = new Form_CreateUser();
        fc.kl = this;
        fc.first = false;
        fc.setVisible(true);
    }//GEN-LAST:event_btnNewUserActionPerformed

    private void btnRollActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRollActionPerformed
        // TODO add your handling code here:
        btnRoll.setEnabled(false);
        String d[] = {"d_Kla.png", "d_Klok.png", "d_Mon.png", "d_Kea.png", "d_Kdam.png", "d_Fish.png"};
        dice_1.setIcon(new ImageIcon(getClass().getResource(d[1])));
        dice_2.setIcon(new ImageIcon(getClass().getResource(d[1])));
        dice_3.setIcon(new ImageIcon(getClass().getResource(d[1])));
        jan.setLocation(1900, 380);
        dice_1.setLocation(2010, 420);
        dice_2.setLocation(2070, 530);
        dice_3.setLocation(190, 530);
        ControlDice(true);
        Thread t = new Thread(() -> {
            try {
                for (int x = 1900; x >= 800; x -= 10) {
                    jan.setLocation(x, 380);
                    dice_1.setLocation(x + 110, 420);
                    dice_2.setLocation(x + 170, 530);
                    dice_3.setLocation(x + 40, 530);
                    Thread.sleep(8);
                }
                Thread.sleep(1000);
                janUp.setVisible(true);
                ControlDice(false);
                long delay = 30;
                Random r = new Random();
                int[] dice = new int[3];
                for (int i = 0; i <= 20; i++) {
                    janUp.setIcon(new ImageIcon(getClass().getResource("jan1.png")));
                    Thread.sleep(delay);
                    janUp.setIcon(new ImageIcon(getClass().getResource("jan2.png")));
                    Thread.sleep(delay);
                    janUp.setIcon(new ImageIcon(getClass().getResource("jan1.png")));
                    Thread.sleep(delay);
                    janUp.setIcon(new ImageIcon(getClass().getResource("jan4.png")));
                    Thread.sleep(delay);
                    dice[0] = r.nextInt(6);
                    dice[1] = r.nextInt(6);
                    dice[2] = r.nextInt(6);
                }
//                //temp
//                dice[0] = 0;
//                dice[1] = 1;
//                dice[2] = 2;
//
//                //end temp
                dice_1.setIcon(new ImageIcon(getClass().getResource(d[dice[0]])));
                dice_2.setIcon(new ImageIcon(getClass().getResource(d[dice[1]])));
                dice_3.setIcon(new ImageIcon(getClass().getResource(d[dice[2]])));
                janUp.setVisible(false);
                int trov = 0;
                long money = 0;
                long templuy;
                long templuyboss = Long.parseLong(MoneyBoss.getName());;
                JLabel[][] templb = new JLabel[3][];
                templb[0] = lb1;
                templb[1] = lb2;
                templb[2] = lb3;
                //JOptionPane.showMessageDialog(rootPane, dice[0] + " , " + dice[1] + " , " + dice[2]);
                for (int i = 0; i < countUser; i++) {
                    for (int j = 1; j < 7; j++) {
                        for (int z = 0; z < 3; z++) {
                            if (!"0".equals(templb[i][j].getText())) {
                                if (dice[z] == j - 1) {
                                    trov++;
                                }
                            }
                        }
                        if (trov > 0) {
                            money += Integer.parseInt(templb[i][j].getText()) * (trov + 1);
                            templuy = Long.parseLong(templb[i][7].getName());
                            templuy += money;
                            templb[i][7].setName(templuy + "");
                            templuyboss -= (Integer.parseInt(templb[i][j].getText()) * trov);
                        } else {
                            //money-=Integer.parseInt(templb[i][j].getText());
                            //templuyboss=Long.parseLong(MoneyBoss.getText());
                            templuyboss += Integer.parseInt(templb[i][j].getText());

                        }
                        MoneyBoss.setName(templuyboss + "");
                        MoneyBoss.setText("Money :" + templuyboss);
                        templb[i][j].setText("0");
                        trov = 0;
                        money = 0;
                    }
                    templb[i][7].setText("Money :" + templb[i][7].getName());
                }
                ThreadChecking.luyboss = Long.parseLong(MoneyBoss.getName());
                ThreadChecking.totalluyjak = 0;
                ControlDice(true);
                Thread.sleep(5000);
                ControlDice(false);
                btnRoll.setEnabled(true);
            } catch (Exception e) {
            }
        });
        t.start();
        ThreadChecking.dicejak = false;
    }//GEN-LAST:event_btnRollActionPerformed

    public void modifyuser(JLabel username, JLabel money, JLabel usertable) {
        if (ThreadChecking.dicejak) {
            JOptionPane.showMessageDialog(rootPane, "Can not Modify while Jak!!!");
        } else {
            Form_CreateUser fc = new Form_CreateUser();
            fc.kl = this;
            fc.setmodify(username, money, usertable);
            fc.setVisible(true);
        }
    }

    private void u2_chairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_u2_chairMouseClicked
        // TODO add your handling code here:
        modifyuser(u2Name, u2Money, u2l);
    }//GEN-LAST:event_u2_chairMouseClicked

    private void u1_chairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_u1_chairMouseClicked
        // TODO add your handling code here:
        modifyuser(u1Name, u1Money, u1l);
    }//GEN-LAST:event_u1_chairMouseClicked

    private void u3_chairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_u3_chairMouseClicked
        // TODO add your handling code here:
        modifyuser(u3Name, u3Money, u3l);
    }//GEN-LAST:event_u3_chairMouseClicked

    private void ChairBossMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChairBossMouseClicked
        // TODO add your handling code here:
        modifyuser(NameBoss, MoneyBoss, null);
    }//GEN-LAST:event_ChairBossMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Kla_Klok_Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Kla_Klok_Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Kla_Klok_Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Kla_Klok_Form.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Kla_Klok_Form().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ChairBoss;
    private javax.swing.JLabel Chicken;
    private javax.swing.JLabel Crab;
    private javax.swing.JLabel Fish;
    private javax.swing.JLabel Gourd;
    private javax.swing.JLabel Lobster;
    private javax.swing.JLabel MoneyBoss;
    private javax.swing.JLabel NameBoss;
    private javax.swing.JLabel Tiger;
    private javax.swing.JButton btnNewUser;
    private javax.swing.JButton btnRoll;
    private javax.swing.JLabel dice_1;
    private javax.swing.JLabel dice_2;
    private javax.swing.JLabel dice_3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel_DL_1000;
    private javax.swing.JLabel jLabel_DL_20000;
    private javax.swing.JLabel jLabel_DL_5000;
    private javax.swing.JLabel jLabel_Fish;
    private javax.swing.JLabel jLabel_Kdam;
    private javax.swing.JLabel jLabel_Kea;
    private javax.swing.JLabel jLabel_Kla;
    private javax.swing.JLabel jLabel_Klok;
    private javax.swing.JLabel jLabel_LL_1000;
    private javax.swing.JLabel jLabel_LL_20000;
    private javax.swing.JLabel jLabel_LL_5000;
    private javax.swing.JLabel jLabel_Mon;
    private javax.swing.JLabel jLabel_RL_1000;
    private javax.swing.JLabel jLabel_RL_20000;
    private javax.swing.JLabel jLabel_RL_5000;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jan;
    private javax.swing.JLabel janUp;
    private javax.swing.JLabel u1Money;
    private javax.swing.JLabel u1Name;
    private javax.swing.JLabel u1_chair;
    private javax.swing.JLabel u1l;
    private javax.swing.JLabel u1l_Lobster;
    private javax.swing.JLabel u1l_chicken;
    private javax.swing.JLabel u1l_crab;
    private javax.swing.JLabel u1l_fish;
    private javax.swing.JLabel u1l_gourd;
    private javax.swing.JLabel u1l_tiger;
    private javax.swing.JLabel u2Money;
    private javax.swing.JLabel u2Name;
    private javax.swing.JLabel u2_chair;
    private javax.swing.JLabel u2l;
    private javax.swing.JLabel u2l_Lobster;
    private javax.swing.JLabel u2l_chicken;
    private javax.swing.JLabel u2l_crab;
    private javax.swing.JLabel u2l_fish;
    private javax.swing.JLabel u2l_gourd;
    private javax.swing.JLabel u2l_tiger;
    private javax.swing.JLabel u3Money;
    private javax.swing.JLabel u3Name;
    private javax.swing.JLabel u3_chair;
    private javax.swing.JLabel u3l;
    private javax.swing.JLabel u3l_Lobster;
    private javax.swing.JLabel u3l_chicken;
    private javax.swing.JLabel u3l_crab;
    private javax.swing.JLabel u3l_fish;
    private javax.swing.JLabel u3l_gourd;
    private javax.swing.JLabel u3l_tiger;
    // End of variables declaration//GEN-END:variables
}
